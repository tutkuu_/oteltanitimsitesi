﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication13.Models;

namespace WebApplication13
{
    public partial class Create : System.Web.UI.Page
    {
        private OtelTanıtımEntities1 db = new OtelTanıtımEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KullanıcıAdı"] == null)
            {
                Response.Redirect("/Giris.aspx");
            }
        }

        protected void btnOlustur_Click(object sender, EventArgs e)
        {
            Oteller ekle = new Oteller
            {
                OtelAdı=txtolustur.Text
            };
            db.Oteller.Add(ekle);
            db.SaveChanges();
            Response.Write("<script>alert('Başarıyşa Oluşturuldu.')</script>");
        }
    }
}