﻿using System;
using System.Linq;
using WebApplication13.Models;

namespace WebApplication13.AdminPanel
{
    public partial class Giris : System.Web.UI.Page
    {
        private OtelTanıtımEntities1 db = new OtelTanıtımEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if( Session["KullanıcıAdı"]!=null)
            {
                Response.Redirect("/Home.aspx");
            }
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            var kullanıcı = db.Kullanıcı.FirstOrDefault(x => x.KullanıcıAdı == TextBox1.Text & x.Şifre == TextBox2.Text);
            Session["KullanıcıAdı"] = kullanıcı.KullanıcıAdı;
            Response.Redirect("/Home.aspx");
        }
    }
}