﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication13.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class OtelTanıtımEntities2 : DbContext
    {
        public OtelTanıtımEntities2()
            : base("name=OtelTanıtımEntities2")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ErkenRezervasyon> ErkenRezervasyon { get; set; }
        public virtual DbSet<Kullanıcı> Kullanıcı { get; set; }
        public virtual DbSet<Oteller> Oteller { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TatilKoyu> TatilKoyu { get; set; }
        public virtual DbSet<YurtDısıOteli> YurtDısıOteli { get; set; }
        public virtual DbSet<Roltürleri> Roltürleri { get; set; }
    }
}
