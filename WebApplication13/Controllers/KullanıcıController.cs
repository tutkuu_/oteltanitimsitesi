﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication13.Models;

namespace WebApplication13.Controllers
{
    public class KullanıcıController : Controller
    {
        private OtelTanıtımEntities1 db = new OtelTanıtımEntities1();
        [Authorize]
       // GET: Kullanıcı
        public ActionResult Index()
        {
            return View(db.Kullanıcı.ToList());
        }
        public ActionResult Giris()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Giris(Kullanıcı Model)
        {
            var kullanıcı = db.Kullanıcı.FirstOrDefault(x => x.KullanıcıAdı == Model.KullanıcıAdı && x.Şifre == Model.Şifre);
            if (kullanıcı != null)
            {
                Session["KullanıcıAdı"] = kullanıcı;
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Hata("Kullanıcı Adı veya Şifre Yanlış");
            return View();
        }
        public ActionResult Cıkıs()
        {
            Session["KullanıcıAdı"] = null;
            RedirectToAction("Index", "Home");
            return View();
        }

        // GET: Kullanıcı/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanıcı = db.Kullanıcı.Find(id);
            if (kullanıcı == null)
            {
                return HttpNotFound();
            }
            return View(kullanıcı);
        }

        // GET: Kullanıcı/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kullanıcı/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UyeId,Ad,Soyad,KullanıcıAdı,Şifre,Eposta")] Kullanıcı kullanıcı)
        {
            if (ModelState.IsValid)
            {
                db.Kullanıcı.Add(kullanıcı);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kullanıcı);
        }

        // GET: Kullanıcı/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanıcı = db.Kullanıcı.Find(id);
            if (kullanıcı == null)
            {
                return HttpNotFound();
            }
            return View(kullanıcı);
        }

        // POST: Kullanıcı/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UyeId,Ad,Soyad,KullanıcıAdı,Şifre,Eposta")] Kullanıcı kullanıcı)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanıcı).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kullanıcı);
        }

        // GET: Kullanıcı/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanıcı kullanıcı = db.Kullanıcı.Find(id);
            if (kullanıcı == null)
            {
                return HttpNotFound();
            }
            return View(kullanıcı);
        }

        // POST: Kullanıcı/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanıcı kullanıcı = db.Kullanıcı.Find(id);
            db.Kullanıcı.Remove(kullanıcı);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
