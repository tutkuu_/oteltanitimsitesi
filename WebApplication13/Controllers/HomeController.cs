﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication13.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult TatilKoyu()
        {
            return View();
        }
        public ActionResult AliBey()
        {
            return View();
        }
        public ActionResult Silence()
        {
            return View();
        }
        public ActionResult Rixos()
        {
            return View();
        }
        public ActionResult Sentido()
        {
            return View();
        }
        public ActionResult Voyage()
        {
            return View();
        }
        public ActionResult Liberty()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}