﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication13.Models;

namespace WebApplication13
{
    public partial class Delete : System.Web.UI.Page
    {
        OtelTanıtımEntities1 db = new OtelTanıtımEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KullanıcıAdı"] == null)
            {
                Response.Redirect("/Giris.aspx");
            }
            try
            {
                string Id = Request.QueryString["ID"];
                int otelId = int.Parse(Id);
                var otel = db.Oteller.First(x => x.Id == otelId);
                db.Oteller.Remove(otel);
                db.SaveChanges();
                Response.Write("<script>alert('Başarıyla Silindi.')</script>");
            }
            catch(Exception)
            {
                Response.Redirect("/Home.aspx");
                
            }
        }
    }
}