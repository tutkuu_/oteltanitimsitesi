﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication13.Models;

namespace WebApplication13.AdminPanel
{
    public partial class Home : System.Web.UI.Page
    {
        OtelTanıtımEntities1 db = new OtelTanıtımEntities1();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["KullanıcıAdı"] == null)
            {
                Response.Redirect("/Home.aspx");
            }
            Repeater1.DataSource = db.Oteller.ToList();
            Repeater1.DataBind();
        }
    }
}